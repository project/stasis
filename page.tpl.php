<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta name="author" content="Chris Herberte http://www.xweb.com.au" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
  <div id="container" class="clear-block">

    <div id="header" class="clear-block">
      <div id="logo">
          <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="Home"><img src="<?php print $logo ?>" alt="Home" /></a><?php } ?>
      </div>
      <div id="header-right">
        <div id="sitename">
          <?php if ($site_name) { ?><a href="<?php print $base_path ?>" title="Home"><?php print $site_name ?></a><?php } ?>
        </div>
        <div id="slogan">
          <?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?>
        </div>
        <div id="primary">
          <?php if ($primary_links) { ?><?php print theme('links', $primary_links) ?><?php } ?>
        </div>
      </div>
    </div>

    <div id="sidebar-container">
      <div id="sidebar">
        <?php if ($left) { ?><?php print $left ?><?php } ?>
        <?php if ($right) { ?><?php print $right ?><?php } ?>
      </div>
      <div id="sidebar-footer">
      </div>
    </div>

    <div id="content">
      <?php print $breadcrumb; ?> 
      <?php if ($show_messages) { print $messages; } ?>
      <h2 class="title"><?php print $title ?></h2>
      <?php print $help ?>
      <?php print $tabs ?>
      <?php print $content; ?> 
      <?php print $feed_icons ?>
    </div>

    <div class="clear-block">
    </div>

      <div id="footer">
        <?php print $footer ?>
        <?php if (isset($secondary_links)) { ?>
        <?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'seclink')) ?>
        <?php } ?>
        <span id="designby">
          <?php if ($site_name) { ?>&copy; <?php print date('Y') ." ". $site_name ?> : <?php } ?>Design by <a href="http://www.xweb.com.au">Chris Herberte</a>
        </span>
      </div>
  </div>
  <?php print $closure ?>

</body>
</html>